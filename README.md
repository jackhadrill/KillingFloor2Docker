# Killing Floor 2 - Docker

Dockerised Killing Floor 2 server.

Run with the following environment variables:

```
SERVER_NAME       (default: KillingFloor2)
MOTD              (default: KillingFloor)
WEBSITE_LINK      (default: none)
PASSWORD          (default: none)
ADMIN_PASSWORD    (default: VerySecurePassword)
WEB_PASSWORD      (default: none)
MAX_PLAYERS       (default: 12)
GAME_DIFFICULTY   (default: 1)
```

Example `docker-compose.yml` file:

```yml
version: '3'
services:
  killingfloor2:
    image: docker.nexus.jacknet.io/jackhadrill/games/killingfloor2:latest
    ports:
      - '7777:7777/udp'
      - '27015:27015/tcp'
      - '8080:8080/tcp'
    environment:
      SERVER_NAME: 'JackNet'
      MOTD: 'Welcome to the JackNet KF2 server.'
      WEBSITE_LINK: 'https://jacknet.io/'
      PASSWORD: password123
      ADMIN_PASSWORD: password123
```

Mount `/game` if you want to persist the server files. Else, it will download on each launch.