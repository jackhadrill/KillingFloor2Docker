#!/bin/bash

ServerName="${SERVER_NAME:-KillingFloor2}"
Motd="${MOTD:-KillingFloor2}"
WebsiteLink="${WEBSITE_LINK:-}"
Password="${PASSWORD:-}"
AdminPassword="${ADMIN_PASSWORD:-VerySecurePassword}"
WebPassword="${WEB_PASSWORD:-}"
MaxPlayers="${MAX_PLAYERS:-12}"
GameDifficulty="${GAME_DIFFICULTY:-1}"

# Update server.
steamcmd +login anonymous +force_install_dir /game +app_update 232130 validate +exit

# Generate default config.
/game/Binaries/Win64/KFGameSteamServer.bin.x86_64 exit

# Patch config files.
sed -i "s/GamePassword=.*/GamePassword=${Password}/g" /game/KFGame/Config/LinuxServer-KFGame.ini
sed -i "s/AdminPassword=.*/AdminPassword=${AdminPassword}/g" /game/KFGame/Config/LinuxServer-KFGame.ini
sed -i "s/MaxPlayers=.*/MaxPlayers=${MaxPlayers}/g" /game/KFGame/Config/LinuxServer-KFGame.ini
sed -i "s/GameDifficulty=.*/GameDifficulty=${GameDifficulty}/g" /game/KFGame/Config/LinuxServer-KFGame.ini
sed -i "s/ServerMOTD=.*/ServerMOTD=${Motd}/g" /game/KFGame/Config/LinuxServer-KFGame.ini
sed -i "s/WebsiteLink=.*/WebsiteLink=${WebsiteLink}/g" /game/KFGame/Config/LinuxServer-KFGame.ini
sed -i "s/ServerName=.*/ServerName=${ServerName}/g" /game/KFGame/Config/LinuxServer-KFGame.ini
sed -i "s/ShortName=.*/ShortName=${ServerName}/g" /game/KFGame/Config/LinuxServer-KFGame.ini
sed -i "s/bEnabled=.*/bEnabled=true/g" /game/KFGame/Config/KFWeb.ini

# Run
/game/Binaries/Win64/KFGameSteamServer.bin.x86_64
