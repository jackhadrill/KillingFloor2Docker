FROM steamcmd/steamcmd:ubuntu

RUN apt-get update && apt-get install -y libcurl4 && apt-get clean

WORKDIR /game
COPY start.sh /start.sh

ENTRYPOINT [ "bash" ]
CMD [ "/start.sh" ]